package main

import (
	"bufio"
	"fmt"
	. "gitlab.com/comalice/ptscheme"
	"os"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Welcome to Pure Tombstone Scheme. Use Ctrl-c to exit.")

	for {
		fmt.Print("> ")
		text, _ := reader.ReadString('\n')
		// make it work on Windows, convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)

		fmt.Println(Evaluate(Parse(Tokenize(text))))
	}
}
