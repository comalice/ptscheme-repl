# What is PT Scheme?
PT stands for "Pure Tombstone". PT Scheme is an R5RS scheme implemented in [Golang](https://golang.org/) with [WASM](https://webassembly.org/) as its compilation target.
# Roadmap
## v0.1.0
This is P. Michaux's "[Bootstrap Scheme](http://peter.michaux.ca/articles/scheme-from-scratch-introduction)".

- tokenizer; take in a string, poop out delimited tokens
- parser, take in a list of delimited tokens, poop out an AST
- interpreter, take in an AST, poop out a result

## v0.2.0
We enter the world of compilers here, but instead of following Michaux into the land of i386 we'll be web-bound and target Web Assembly instead.

## v0.3.0
Compile our Scheme to Golang.

## v0.4.0
Compile our Scheme to a byte code VM.

## v0.5.0
Add a JIT engine to our WASM compiler.

## v0.6.0 and beyond
Dunno, yet. We'll see when we get there?
